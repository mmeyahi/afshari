import React from 'react';
import {AsyncStorage, Image, StatusBar, StyleSheet} from "react-native";
import {Button, Container, Input, Spinner, Text, View} from 'native-base';

import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/Entypo';
import {Colors, HOST} from "../config/Utility";
import SplashScreen from "react-native-splash-screen";
import BG from '../assets/general/bg.jpg';

let splashTimer = null;

class LoginComponent extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            btnSpinner: false,
            mobileSet: false,
            mobile: null,
        };
    }

    componentWillMount() {
        this.autoLogin()
            .then(res => {
                    if (res) {
                        AsyncStorage.setItem('@SchoolService:userData', JSON.stringify(res.data));
                        this.props.changeState(res.status);
                    }
                    splashTimer = setTimeout(() => {
                        SplashScreen.hide();
                    }, 500);
                }
            );
    }

    componentWillUnmount() {
        clearTimeout(splashTimer);
    }

    async autoLogin() {

        const mobile = await AsyncStorage.getItem('@SchoolService:mobile');
        const apiPass = await AsyncStorage.getItem('@SchoolService:autoPass');

        if (!mobile || !apiPass) {
            return false;
        }

        return await fetch(HOST + 'login', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            credentials: 'same-origin',
            body: JSON.stringify({
                mobile,
                apiPass
            }),
        })
            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.status) {
                    return {status: 3, data: responseJson.data};
                }
                return false;
            })
            .catch((error) => {
                return false;
            });
    }

    async getVerifyCode() {

        const mobile = this.state.mobile;

        return await fetch(HOST + 'verify-get', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            credentials: 'same-origin',
            body: JSON.stringify({mobile}),
        })
            .then((response) => response.json())
            .then((responseJson) => {
                console.log(responseJson);
                if (responseJson.status) {
                    return mobile;
                }
                return false;
            });
    }

    render() {
        return (
            <Container style={styles.container}>
                <StatusBar backgroundColor={Colors.COLOR_STATUS_BAR}/>
                <Image style={{flex: 1, position: 'absolute', top: 0, height: '100%', width: '100%'}} source={BG}/>

                <Text style={styles.help}>
                    جهت ورود مجدد یا ثبت نام شماره همراه خود را در فیلد زیر وارد کنید. شماره همراه باید با صفر شروع شود و پس از ورود کامل
                    شماره همراه روی دکمه مجاور کلیک کنید.
                </Text>

                <View style={styles.mobileFrame}>
                    <View style={styles.btnFrame}>
                        <Button
                            style={[styles.btnMobileSubmit, {backgroundColor: this.state.mobileSet ? '#5cb85c' : '#122638'}]}
                            rounded
                            onPress={() => {
                                if (this.state.mobileSet) {
                                    this.setState({
                                        btnSpinner: true,
                                    });
                                    this.getVerifyCode()
                                        .then((mobile) => {
                                            this.setState({
                                                btnSpinner: false
                                            });
                                            if (mobile) {
                                                AsyncStorage.setItem('@SchoolService:mobile', mobile);
                                                this.props.changeState(2);
                                            }
                                        });
                                }
                            }}
                        >
                            {!this.state.btnSpinner && !this.state.mobileSet &&
                            <Icon name="edit" size={24} color="#fff"/>}
                            {!this.state.btnSpinner && this.state.mobileSet &&
                            <Icon name="check" size={24} color="#fff"/>}
                            {this.state.btnSpinner && <Spinner/>}
                        </Button>
                    </View>
                    <Input
                        placeholder="شماره همراه"
                        placeholderTextColor={Colors.COLOR_BASE_DARK}
                        maxLength={11}
                        onChangeText={(mobile) => {
                            if (mobile.length === 11) {
                                this.setState({
                                    mobileSet: true,
                                    mobile
                                });
                            } else {
                                this.setState({
                                    mobileSet: false
                                });
                            }
                        }}
                        keyboardType="numeric"
                        style={styles.mobileInput}/>
                </View>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    mobileFrame: {
        flexDirection: 'row',
        padding: 5,
        backgroundColor: '#09aaa299',
        width: 320,
        height: 60,
        borderRadius: 30
    },
    mobileInput: {
        textAlign: 'center',
        fontFamily: 'sans',
        fontSize: 20,
        height: 55,
        color: Colors.COLOR_BASE_LIGHT
    },
    btnFrame: {
        width: 50,
        height: 50
    },
    btnMobileSubmit: {
        width: 50,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center'
    },
    help: {
        margin: 32,
        textAlign: 'center',
        fontFamily: 'sans',
        fontSize: 12,
        color: Colors.COLOR_BASE_DARK
    }
});

LoginComponent.propTypes = {
    changeState: PropTypes.func.isRequired
};

export default LoginComponent;
