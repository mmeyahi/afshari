import React from 'react';

import {
	BackAndroid,
    AsyncStorage,
	Image,
    StyleSheet,
} from 'react-native';

import {
    Container,
    Content,
    Body,
    View,
    Text,
    Thumbnail, Button, Right,
} from 'native-base';
import dIcon from '../assets/icons/d_exit.png';

import { DrawerItems } from 'react-navigation';
import jMoment from 'jalali-moment';
import iMoment from 'moment-hijri';
import moment from 'moment';
import 'moment/locale/fa';
moment.locale('fa');

import { Colors, API_MEDIA_AVATAR } from '../config/Utility';
import drawerBG from '../assets/general/bg.jpg';

class DrawerContent extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			user: ''
		};
	}

	async fetchUserData() {
		return await AsyncStorage.getItem('@SchoolService:userData');
	}

	componentWillMount() {
		this.fetchUserData()
			.then(data => {
				const {user} = JSON.parse(data);

				this.setState({
					user
				});
			});
	}

	render() {
		return (
			<Container>
				 <Image style={{ flex: 1, position: 'absolute', top: 0, height: '100%', width: '100%' }} source={drawerBG} />
				<Content>
					<View
						style={{
							alignItems: 'center',
							justifyContent: 'center',
							padding: 8
						}}
					>
						<View style={{
							flexDirection: 'row-reverse', alignItems: 'center', paddingBottom: 8, marginBottom: 8
						}}>
							<Thumbnail
								source={{ uri: API_MEDIA_AVATAR + this.state.user.avatar }}
								style={{ borderWidth: 3, borderColor: '#122638', backgroundColor: '#09aaa2' }}
							/>
							<Body style={{alignItems: 'flex-end', padding: 12}}>
								<Text style={styles.text}>
									{this.state.user.fname + ' ' + this.state.user.lname}
								</Text>
								<Text style={styles.text}>
									{this.state.user.mobile}
								</Text>
							</Body>
						</View>

						<Body style={styles.calendar}>
							<Text style={[styles.text, { borderBottomColor: Colors.COLOR_BASE_LIGHT2_GLAS, borderBottomWidth: 1, width: 120, marginBottom: 16, textAlign: 'center' }]}>
								{jMoment('2018/01/24', 'YYYY/MM/DD').locale('fa').format('dddd')}
							</Text>
							<View style={{
								width: 240, flexDirection: 'row-reverse', justifyContent: 'space-between',
								paddingBottom: 2, marginBottom: 2
							}}>
								<Text style={styles.text}>
									{jMoment().locale('fa').format('jD')}
								</Text>
								<Text style={styles.text}>
									{jMoment().locale('fa').format('jMMMM')}
								</Text>
								<Text style={styles.text}>
									{jMoment().locale('fa').format('jYYYY')}
								</Text>
							</View>
							<View style={{
								width: 240, flexDirection: 'row-reverse', justifyContent: 'space-between', paddingBottom: 2,
								marginBottom: 2
							}}>
								<Text style={styles.text}>
									{iMoment().locale('ar-sa').format('iD')}
								</Text>
								<Text style={styles.text}>
									{iMoment().locale('ar-sa').format('iMMMM')}
								</Text>
								<Text style={styles.text}>
									{iMoment().locale('ar-sa').format('iYYYY')}
								</Text>
							</View>
							<View style={{ width: 240, flexDirection: 'row-reverse', justifyContent: 'space-between' }}>
								<Text style={styles.text}>
									{moment().format('D')}
								</Text>
								<Text style={styles.text}>
									{moment().format('MMMM')}
								</Text>
								<Text style={styles.text}>
									{moment().format('YYYY')}
								</Text>
							</View>
						</Body>
					</View>
					<DrawerItems {...this.props} />
					<Button
						full
						style={{
							backgroundColor: '#09aaa2', marginTop: -4, height: 50
						}}

						onPress={() => {
                            AsyncStorage.clear();
							BackAndroid.exitApp();
						}}
					>
                        <Text style={[styles.text, {width: 220}]}>خروج</Text>
						<Right>
                            <Image style={{height: 28, width: 28, margin: 12}} source={dIcon}/>
						</Right>
					</Button>
				</Content>
			</Container>
		);
	}
}

const styles = StyleSheet.create({
	text: {
		fontSize: 14,
		fontFamily: 'sans'
	},
	calendar: {
		backgroundColor: '#09aaa2cc',
		padding: 16,
		borderRadius: 4,
	}
});


export default DrawerContent;