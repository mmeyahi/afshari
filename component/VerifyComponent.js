import React from 'react';
import {AsyncStorage, I18nManager, Image, StatusBar, StyleSheet} from "react-native";
import {Button, Container, Spinner, Text, View} from 'native-base';
import PinInput from 'react-native-pin-input';
import PropTypes from 'prop-types';
import Ionicons from 'react-native-vector-icons/Ionicons';
import SmsListener from "react-native-android-sms-listener";
import Utility, {Colors, HOST} from "../config/Utility";
import BG from '../assets/general/bg.jpg';

let counter = null;

class VerifyComponent extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            btnSpinner: false,
            alowRefresh: false,
            timer: 120,
            pin: '',
            verifyCode: ''
        };
    }

    componentDidMount() {
        SmsListener.addListener(message => {
            console.log(message);
            if (message.originatingAddress === '5000289928') {
                const verifyCode = message.body.match(/\d+/)[0];
                this.setState({
                    verifyCode,
                    btnSpinner: true
                });

                this.userVerify()
                    .then(res => {
                        if (res) {
                            AsyncStorage.setItem('@SchoolService:autoPass', res.autoPass);
                            if (res.data) {
                                AsyncStorage.setItem('@SchoolService:userData', JSON.stringify(res.data));
                            }
                            this.props.changeState(res.status);
                        }

                        this.setState({
                            btnSpinner: false
                        });
                    });
            }
        });

        counter = setInterval(() => {
            const t = this.state.timer - 1;

            this.setState({
                timer: t
            });

            if (t === 0) {
                clearInterval(counter);
                this.setState({
                    alowRefresh: true
                });
            }
        }, 1000);
    }

    componentWillUnmount() {
        clearInterval(counter);
    }

    async userVerify() {

        const verifyCode = this.state.verifyCode;
        const autoPass = Utility.passGen(32);

        return await fetch(HOST + 'verify', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            credentials: 'same-origin',
            body: JSON.stringify({
                verifyCode,
                autoPass: Utility.passGen(32)
            }),
        })
            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.status) {
                    return {autoPass, data: responseJson.data, status: 3};
                }
                return false;
            });
    }

    render() {
        return (
            <Container style={styles.container}>
                <StatusBar backgroundColor={Colors.COLOR_STATUS_BAR}/>
                <Image style={{flex: 1, position: 'absolute', top: 0, height: '100%', width: '100%'}} source={BG}/>

                <Text style={styles.help}>
                    کد تایید ۵ رقمی از طریق پیام کوتاه برای شما ارسال شد. پس از دریافت آن را در فیلد زیر وارد کنید.
                </Text>

                <View style={styles.verifyFrame}>
                    <Button
                        style={styles.btnMobileSubmit}
                        success
                        rounded
                        onPress={() => {
                            if (this.state.alowRefresh) {
                                this.props.changeState(1);
                            }
                        }}
                    >
                        {!this.state.btnSpinner && <Ionicons name="ios-refresh" size={48} color="#fff"/>}
                        {this.state.btnSpinner && <Spinner/>}
                    </Button>
                    <PinInput
                        ref={'pin'}
                        pinLength={5}
                        autoFocus={true}
                        value={this.state.pin}
                        pinItemStyle={styles.pinItem}
                        pinItemProps={{keyboardType: 'numeric'}}
                        onPinCompleted={(verifyCode) => {
                            this.setState({
                                verifyCode,
                                btnSpinner: true
                            });

                            this.userVerify()
                                .then(res => {
                                    if (res) {
                                        AsyncStorage.setItem('@SchoolService:autoPass', res.autoPass);
                                        if (res.data) {
                                            AsyncStorage.setItem('@SchoolService:userData', JSON.stringify(res.data));
                                        }
                                        this.props.changeState(res.status);
                                    }

                                    this.setState({
                                        btnSpinner: false
                                    });
                                });
                        }}
                        style={styles.pinBox}
                    />
                </View>

                <Text style={styles.help}>
                    درصورتی که کد تایید پس از
                    <Text style={{width: 50, textAlign: 'center', color: 'blue'}}> {this.state.timer} </Text>
                    ثانیه دریافت نشد می توانید با استفاده از دکمه بالا درخواست کد جدید کنید
                </Text>

            </Container>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    btnMobileSubmit: {
        width: 50,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center'
    },
    verifyFrame: {
        flexDirection: 'row',
        padding: 5,
        backgroundColor: '#09aaa299',
        width: 320,
        height: 60,
        borderRadius: 30
    },
    pinItem: {
        height: 50,
        width: 40,
        fontSize: 28,
        borderWidth: 0,
        color: Colors.COLOR_BASE_LIGHT
    },
    pinBox: {
        flexDirection: I18nManager.isRTL ? 'row-reverse' : 'row',
        marginRight: 16,
        marginLeft: 16
    },
    help: {
        margin: 32,
        textAlign: 'center',
        fontFamily: 'sans',
        fontSize: 12,
        color: Colors.COLOR_BASE_DARK
    }
});

VerifyComponent.propTypes = {
    changeState: PropTypes.func.isRequired
};

export default VerifyComponent;
