/** @format */
import React from 'react';
import {AppRegistry, Dimensions, View} from 'react-native';
import Router from './config/router';
import {appName} from './config/app.json';
import LoginComponent from "./component/LoginComponent";
import VerifyComponent from "./component/VerifyComponent";

const {width} = Dimensions.get('window');

class Root extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            displayState: 1,
            opc: 0
        }
    }

    componentDidMount() {
        // setTimeout(() => {
        //     this.setState({opc: 1})
        // }, 20000)
    }

    main() {
        return (
            <View style={{flex: 1}}>
                <Router/>

                <View style={{
                    opacity: this.state.opc, position: 'absolute', left: 16,
                    bottom: 16, width: width - 32, height: 0,
                    backgroundColor: '#fff', borderRadius: 4, elevation: 8
                }}>

                </View>
            </View>
        )
    }

    display = () => {
        switch (this.state.displayState) {
            case 1:
                return <LoginComponent changeState={(state) => this.setState({displayState: state})}/>;
            case 2:
                return <VerifyComponent changeState={(state) => this.setState({displayState: state})}/>;
            case 3:
                return this.main();
        }
    };

    render() {
        return (
            this.display()
        );
    }
}

AppRegistry.registerComponent(appName, () => Root);
