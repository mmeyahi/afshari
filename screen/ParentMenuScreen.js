import React from 'react';
import {Dimensions, Image, StyleSheet, TouchableHighlight} from 'react-native';
import {Container, Text, View} from 'native-base';
import {NotificationsAndroid, PendingNotifications} from 'react-native-notifications';
import dIcon from '../assets/icons/d_home.png';
import BG from '../assets/general/profile_bg.jpg';
import GridList from "react-native-grid-list";
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import d_send_msg from '../assets/icons/d_send_msg.png';
import d_inbox from '../assets/icons/d_inbox.png';
import d_outbox from '../assets/icons/d_outbox.png';
import d_calendar from '../assets/icons/d_calendar.png';
import d_profile from '../assets/icons/d_profile.png';
import {HOST} from "../config/Utility";

const {height} = Dimensions.get('window');

class ParentMenuScreen extends React.Component {

    static navigationOptions = {
        drawerLabel: 'منوی اصلی',
        drawerIcon: ({tintColor}) => (
            <Image style={{height: 28, width: 28, tintColor}} source={dIcon}/>
        )
    };

    constructor(props) {
        super(props);

        this.state = {
            menu: [
                {
                    label: 'مشاهده نقشه',
                    icon: <SimpleLineIcons name="map" color='#fff' style={{marginLeft: 16, width: 36, height: 32}} size={22}/>,
                    target: 'parentScreen'
                },
                {
                    label: 'ارسال پیام',
                    icon: <Image style={{height: 28, width: 28, tintColor: '#fff'}} source={d_send_msg}/>,
                    target: 'parentMsgScreen'
                },
                {
                    label: 'پیام های دریافتی',
                    icon: <Image style={{height: 28, width: 28, tintColor: '#fff'}} source={d_inbox}/>,
                    target: 'parentInbox'
                },
                {
                    label: 'پیام های ارسال شده',
                    icon: <Image style={{height: 28, width: 28, tintColor: '#fff'}} source={d_outbox}/>,
                    target: 'parentOutbox'
                },
                {
                    label: 'اعلام غیبت',
                    icon: <Image style={{height: 28, width: 28, tintColor: '#fff'}} source={d_calendar}/>,
                    target: 'calendarScreen'
                },
                {
                    label: 'پروفایل',
                    icon: <Image style={{height: 28, width: 28, tintColor: '#fff'}} source={d_profile}/>,
                    target: 'parentProfileScreen'
                },
            ]
        }
    }

    componentDidMount() {
        NotificationsAndroid.setNotificationOpenedListener((notification) => {
            this.props.navigation.navigate('parentInbox');
        });

        this.fetchInboxData().then(respond => {
            if (respond) {
                const msgs = respond.data.filter((msg) => {
                    return (msg.receiver_type == 2 && msg.status == 1)
                });

                if (msgs.length > 0) {
                    NotificationsAndroid.localNotification({
                        title: "پیام جدید",
                        body: "شما به تعداد " + msgs.length + " پیام دریافتی جدید دارید",
                        extra: "data"
                    });
                }
            }
        });
    }

    async fetchInboxData() {

        return await fetch(HOST + 'parent/massage', {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            credentials: 'same-origin',
        })
            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.status) {
                    return responseJson;
                }
                return false;
            }).catch((error) => {
                return false;
            });
    }

    renderItem = ({item, index}) => (
        <TouchableHighlight
            underlayColor='transparent'
            style={styles.gridContainer}
            onPress={() => {
                this.props.navigation.navigate(item.target);
            }}
        >
            <View style={styles.item}>
                {item.icon}
                <Text style={styles.label}>{item.label}</Text>
            </View>
        </TouchableHighlight>
    );

    render() {
        return (
            <Container>
                <Image style={{flex: 1, position: 'absolute', top: 0, height: '100%', width: '100%'}} source={BG}/>
                <View>
                    <GridList
                        showSeparator={false}
                        data={this.state.menu}
                        separatorBorderWidth={1}
                        separatorBorderColor="silver"
                        numColumns={2}
                        renderItem={this.renderItem}
                        itemStyle={styles.gridItem}
                        style={styles.grid}
                    />
                </View>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 32
    },
    grid: {
        marginTop: (height - 580) / 2
    },
    gridContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    gridItem: {
        height: 200
    },
    item: {
        width: 140,
        height: 140,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 16,
        backgroundColor: '#09aaa299',
        borderRadius: 8
    },
    label: {
        marginTop: 8,
        fontFamily: 'sans',
        color: '#fff',
        fontSize: 12
    }
});

export default ParentMenuScreen;
