import React from 'react';
import {AsyncStorage, Dimensions, I18nManager, Modal, StyleSheet, TouchableOpacity} from 'react-native';
import {Body, Button, Container, Fab, Header, Left, Right, Text, Thumbnail, Title, View} from 'native-base';

import GPSState from 'react-native-gps-state';

import WebViewLeaflet from 'react-native-webview-leaflet'
import Icon from 'react-native-vector-icons/SimpleLineIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {API_MEDIA_AVATAR, Colors} from '../config/Utility';
import * as svgIcons from '../assets/general/marker_32_1';

const {width} = Dimensions.get('window');

class DriverScreen extends React.Component {

    static navigationOptions = {
        drawerLabel: 'مشاهده نقشه',
        drawerIcon: ({tintColor}) => (
            <Icon name="map" color={tintColor} style={{marginLeft: 16, width: 36, height: 32}} size={22}/>
        )
    };

    constructor(props) {
        super(props);

        this.state = {
            students: [],
            locations: [],
            student: {},
            modalStdInfo: false,
            locationModal: false
        }
    }

    async fetchUserData() {
        return await AsyncStorage.getItem('@SchoolService:userData');
    }

    componentWillMount(){
        GPSState.getStatus().then((status) => {
            if (status === 1) {
                this.setState({
                    locationModal: true
                })
            }
        });
    }

    componentDidMount() {
        setTimeout(() => {
            this.fetchUserData()
                .then(response => {
                    const json = JSON.parse(response);
                    const locations = [];
                    json.driver.students.map((student, i) => {
                        locations.push({
                            id: student.id,
                            coords: [student.lat, student.lng],
                            icon: svgIcons.std_marker,
                            size: [32, 32]
                        });
                    });

                    this.setState({
                        students: json.driver.students,
                        locations
                    });

                    this.webViewLeaflet.sendMessage({
                        locations: this.state.locations
                    });
                });
        }, 100);
    }

    onLoad = (event) => {
        navigator.geolocation.getCurrentPosition((location) => {
            this.webViewLeaflet.sendMessage({
                zoom: 12,
                centerPosition: [location.coords.latitude, location.coords.longitude]
            });
        });
    };

    onMapMarkerClicked = ({payload}) => {
        this.state.students.map((student, i) => {
            if (student.id == payload.id) {
                this.setState({
                    student,
                    modalStdInfo: !!this.state.student
                })
            }
        });
    };

    render() {
        return (
            <Container>
                <Header style={styles.header}>
                    <Left>
                        <Button
                            transparent
                            onPress={() => this.props.navigation.navigate('driverMenuScreen')}
                        >
                            <Ionicons name="ios-arrow-back" size={32} color="#fff"/>
                        </Button>
                    </Left>
                    <Body>
                    <Title style={styles.headerTitle}>دانش آموزان در نقشه</Title>
                    </Body>
                    <Right>
                        <Button
                            transparent
                            onPress={() => this.props.navigation.toggleDrawer()}
                        >
                            <Ionicons name='ios-menu' size={32} color="#fff"/>
                        </Button>
                    </Right>
                </Header>

                <WebViewLeaflet
                    ref={(component) => (this.webViewLeaflet = component)}
                    centerButton={false}
                    eventReceiver={this}
                />

                {!!this.state.student &&
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.state.modalStdInfo}
                    onRequestClose={() => {
                        this.setState({
                            modalStdInfo: false
                        });
                    }}>
                    <TouchableOpacity
                        onPress={() =>
                            this.setState({
                                modalStdInfo: false
                            })
                        }
                        style={styles.modal}
                    >
                        <View style={{backgroundColor: Colors.COLOR_BASE_LIGHT2}}>
                            <Thumbnail
                                source={{uri: API_MEDIA_AVATAR + this.state.student.avatar}}
                                style={{borderWidth: 3, borderColor: Colors.COLOR_BASE_LIGHT}}
                            />
                        </View>

                        <View style={{padding: 8}}>
                            <Text style={{color: Colors.COLOR_BASE_DARK, fontFamily: 'sans', fontSize: 12}}>
                                {this.state.student.name}
                            </Text>
                            <Text style={{color: Colors.COLOR_BASE_DARK, fontFamily: 'sans', fontSize: 7}}>
                                {this.state.student.address}
                            </Text>
                        </View>

                        <Left>
                            <Icon name="info" color={Colors.COLOR_BASE_DARK2} style={{marginLeft: 16}} size={32}/>
                        </Left>
                    </TouchableOpacity>
                </Modal>
                }

                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.state.locationModal}
                    onRequestClose={() => {
                        this.setState({
                            locationModal: false
                        });
                    }}
                    style={{elevation: 8}}
                >
                    <TouchableOpacity
                        onPress={() => {
                            this.setState({
                                locationModal: false
                            });
                        }}
                        style={{flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#00000033'}}>
                        <View style={{width: width - 64, backgroundColor: Colors.COLOR_BASE_LIGHT, elevation: 4}}>
                            <Text
                                style={[styles.text, {
                                    backgroundColor: '#122638',
                                    color: Colors.COLOR_BASE_LIGHT,
                                    textAlign: 'center'
                                }]}>
                                مکان یاب دستگاه فعال نیست
                            </Text>

                            <View style={{padding: 24}}>
                                <Text style={styles.text}>
                                    برای استفاده از امکانات نقشه باید مکان یاب دستگاه فعال باشد. اکنون فعال شود؟
                                </Text>
                            </View>

                            <View
                                style={{flexDirection: 'row'}}
                            >
                                <Button
                                    style={{width: '50%', borderRadius: 0, backgroundColor: '#122638'}}
                                    onPress={() => {
                                        this.setState({
                                            locationModal: false
                                        });
                                    }}
                                >
                                    <Text style={[styles.text, {textAlign: 'center', color: Colors.COLOR_BASE_LIGHT}]}>خیر</Text>
                                </Button>
                                <Button
                                    style={{width: '50%', borderRadius: 0, justifyContent: 'center', backgroundColor: '#09aaa2'}}
                                    onPress={() => {
                                        this.setState({
                                            locationModal: false
                                        });

                                        GPSState.openSettings();

                                    }}
                                >
                                    <Text style={[styles.text, {textAlign: 'center', color: Colors.COLOR_BASE_LIGHT}]}>
                                        بله
                                    </Text>
                                </Button>
                            </View>
                        </View>
                    </TouchableOpacity>
                </Modal>

                <Fab
                    direction={I18nManager.isRTL ? 'right' : 'left'}
                    style={{backgroundColor: '#09aaa2'}}
                    position={I18nManager.isRTL ? 'bottomLeft' : 'bottomRight'}
                    onPress={() => {
                        navigator.geolocation.getCurrentPosition((location) => {
                            this.webViewLeaflet.sendMessage({
                                centerPosition: [location.coords.latitude + 0.0001, location.coords.longitude + 0.0001]
                            });

                            this.webViewLeaflet.sendMessage({
                                centerPosition: [location.coords.latitude, location.coords.longitude]
                            });
                        });
                    }}>
                    <Ionicons name='md-locate' size={48} color={Colors.COLOR_BASE_DARK} style={{margin: 16}}/>
                </Fab>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.COLOR_BASE_LIGHT
    },
    header: {
        backgroundColor: '#122638'
    },
    headerTitle: {
        fontFamily: 'sans',
        width: width - 140,
        direction: 'rtl',
        textAlign: 'right'
    },
    card: {
        width: width - 16,
        alignSelf: 'center',
    },
    cardItemTop: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: 'rgba(200, 200, 200, 0.8)'
    },
    cardItem: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'flex-end',
        flexDirection: 'row',
        backgroundColor: 'rgba(200, 200, 200, 0.8)'
    },
    textTop: {
        fontSize: 14,
        fontFamily: 'sans',
        padding: 8,
        width: '100%'
    },
    text: {
        fontSize: 14,
        fontFamily: 'sans',
        padding: 16,
        width: '100%'
    },
    modal: {
        flexDirection: 'row-reverse',
        alignItems: 'center',
        backgroundColor: Colors.COLOR_BASE_LIGHT,
        margin: 8,
        elevation: 2,
        marginTop: 70,
    }
});

export default DriverScreen;
