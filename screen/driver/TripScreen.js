import React from 'react';
import {AsyncStorage, Dimensions, I18nManager, Image, Modal, StyleSheet, Text, TouchableOpacity} from 'react-native';
import {Body, Button, Container, Content, Fab, Header, Left, Right, Spinner, Thumbnail, Title, View} from 'native-base';
import {API_MEDIA_AVATAR, Colors, HOST} from '../../config/Utility';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import BG from '../../assets/general/profile_bg.jpg';

import moment from 'moment';

const {width} = Dimensions.get('window');

export default class TripScreen extends React.Component {

    static navigationOptions = {
        drawerLabel: 'گزارش مسافرت',
        drawerIcon: ({tintColor}) => (
            <SimpleLineIcons name="user-following" color='#fff' size={24}/>
        ),
    };

    constructor(props) {
        super(props);

        this.state = {
            modalShow: false,
            tripData: [],
            spinner: false,
            fabActive: false,
            fabSpinner: false,
            modalHelp: false
        }
    }

    componentWillMount() {
        this.fetchUserData()
            .then(data => {
                const userData = JSON.parse(data);

                userData.driver.students.map((student, i) => {

                    console.log(userData);

                    const tripData = this.state.tripData;

                    const trip = {
                        sid: student.id,
                        name: student.name,
                        avatar: student.avatar,
                        off: this.hasOff(student.calendars, moment().locale('en').format('YYYY-MM-DD')),
                        state: 0
                    };

                    if (!!student.trip) {
                        trip.state = this.stateCalc(student.trip.type, student.trip.status);
                        trip.tid = student.trip.id;
                    }

                    tripData.push(trip);
                    this.setState({tripData});
                });

            });
    }

    stateCalc(type, status) {
        if (status == 3) return -1;
        return 2 * (type - 1) + status;
    }

    async fetchUserData() {
        return await AsyncStorage.getItem('@SchoolService:userData');
    }

    hasOff(cals, date) {
        let result = false;
        cals.map((cal, i) => {
            if (cal.offDate == date) result = true;
        });

        return result;
    }

    async storeTrip(trip) {

        return await fetch(HOST + 'driver/trip', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            credentials: 'same-origin',
            body: JSON.stringify({
                sid: trip.sid,
                tid: trip.tid,
                type: trip.type,
                status: trip.status
            }),
        })
            .then((response) => response.json())
            .then((responseJson) => {
                return responseJson;
            });
    }

    asyncStore(trp, state, type, status) {

        const trip = trp;
        trip.type = type;
        trip.status = status;

        this.storeTrip(trip).then(res => {
            if (res.status) {
                const tripData = [];
                const trips = this.state.tripData.map((item, i) => {
                    if (item.sid === trp.sid) {
                        item.state = state;
                        item.tid = res.data.id;
                    }

                    tripData.push(item);
                });

                this.setState({
                    tripData
                })
            } else {
                const tripData = [];
                const trips = this.state.tripData.map((item, i) => {
                    if (item.sid === trp.sid) {
                        item = {
                            sid: item.sid,
                            name: item.name,
                            avatar: item.avatar,
                            off: item.off
                        }
                    }

                    tripData.push(item);
                });

                this.setState({
                    tripData
                })
            }

            this.setState({
                spinner: false,
                fabSpinner: false
            })
        })
    }

    render() {
        return (
            <Container style={{backgroundColor: Colors.COLOR_BASE_LIGHT}}>
                <Image style={{flex: 1, position: 'absolute', top: 0, height: '100%', width: '100%'}} source={BG}/>
                <Header style={styles.header}>
                    <Left>
                        <Button
                            transparent
                            onPress={() => this.props.navigation.goBack()}
                        >
                            <Ionicons name="ios-arrow-back" size={32} color="#fff"/>
                        </Button>
                    </Left>
                    <Left>
                        <Button
                            transparent
                            onPress={() => this.setState({modalHelp: true})}
                        >
                            <Ionicons name="ios-help-circle" size={32} color="#fff"/>
                        </Button>
                    </Left>
                    <Body>
                    <Title style={styles.headerTitle}>گزارش سفر</Title>
                    </Body>
                    <Right>
                        <Button
                            transparent
                            onPress={() => this.props.navigation.toggleDrawer()}
                        >
                            <Ionicons name='ios-menu' size={32} color="#fff"/>
                        </Button>
                    </Right>
                </Header>

                <Content style={{padding: 8}}>
                    {
                        this.state.tripData.map((trip, i) => {
                            return (
                                <View key={i} style={styles.tripBox}>
                                    <View style={styles.itemHeader}>
                                        <Thumbnail
                                            source={{uri: API_MEDIA_AVATAR + trip.avatar}}
                                            style={{borderWidth: 3, borderColor: Colors.COLOR_BASE_LIGHT2}}
                                        />
                                        <Text style={[styles.text, {marginStart: 16, marginEnd: 16}]}>
                                            {trip.name}
                                        </Text>
                                        <Text style={[styles.text, {marginStart: 16, marginEnd: 16, fontSize: 12}]}>
                                            {trip.off ? 'غایب' : 'حاضر'}
                                        </Text>
                                    </View>
                                    <View style={styles.itemControls}>
                                        <Button transparent style={{
                                            width: '20%', justifyContent: 'center', alignItems: 'center', flexDirection: 'column'
                                        }}
                                                onPress={() => {
                                                    this.setState({
                                                        spinner: true
                                                    });
                                                    this.asyncStore(trip, 1, 1, 1);
                                                }}
                                        >
                                            <SimpleLineIcons
                                                name={trip.state > 0 ? 'like' : 'login'}
                                                color={trip.state > 0 ? '#09aaa2' : Colors.COLOR_BASE_DARK2}
                                                size={18}/>
                                            <Text style={styles.text}>شروع</Text>
                                        </Button>
                                        <Button transparent style={{
                                            width: '20%', justifyContent: 'center', alignItems: 'center', flexDirection: 'column'
                                        }}
                                                onPress={() => {
                                                    this.setState({
                                                        spinner: true
                                                    });
                                                    this.asyncStore(trip, 2, 1, 2);
                                                }}
                                        >
                                            <SimpleLineIcons
                                                name={trip.state > 1 ? 'like' : 'logout'}
                                                color={trip.state > 1 ? '#09aaa2' : Colors.COLOR_BASE_DARK2}
                                                size={18}/>
                                            <Text style={styles.text}>پایان</Text>
                                        </Button>


                                        <Button transparent style={{
                                            width: '20%', justifyContent: 'center', alignItems: 'center', flexDirection: 'column'
                                        }}
                                                onPress={() => {
                                                    this.setState({
                                                        spinner: true
                                                    });
                                                    this.asyncStore(trip, 3, 2, 1);
                                                }}
                                        >
                                            <SimpleLineIcons
                                                name={trip.state > 2 ? 'like' : 'login'}
                                                color={trip.state > 2 ? '#09aaa2' : Colors.COLOR_BASE_DARK2}
                                                size={18}/>
                                            <Text style={styles.text}>شروع</Text>
                                        </Button>
                                        <Button transparent style={{
                                            width: '20%', justifyContent: 'center', alignItems: 'center', flexDirection: 'column'
                                        }}
                                                onPress={() => {
                                                    this.setState({
                                                        spinner: true
                                                    });
                                                    this.asyncStore(trip, 4, 2, 2);
                                                }}
                                        >
                                            <SimpleLineIcons
                                                name={trip.state > 3 ? 'like' : 'logout'}
                                                color={trip.state > 3 ? '#09aaa2' : Colors.COLOR_BASE_DARK2}
                                                size={18}/>
                                            <Text style={styles.text}>پایان</Text>
                                        </Button>


                                        <Button transparent style={{
                                            width: '20%', justifyContent: 'center', alignItems: 'center', flexDirection: 'column'
                                        }}
                                                onPress={() => {
                                                    this.setState({
                                                        spinner: true
                                                    });
                                                    this.asyncStore(trip, -1, 0, 3);
                                                }}
                                        >
                                            <SimpleLineIcons
                                                name={trip.state === -1 ? 'like' : 'user-unfollow'}
                                                color={trip.state === -1 ? '#09aaa2' : Colors.COLOR_BASE_DARK2}
                                                size={18}/>
                                            <Text style={styles.text}>غیبت</Text>
                                        </Button>
                                    </View>
                                </View>
                            )
                        })
                    }
                </Content>

                <Modal
                    animationType='fade'
                    transparent
                    onRequestClose={() => this.setState({spinner: false})}
                    visible={this.state.spinner}
                >
                    <View style={{
                        flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#00000099'
                    }}>
                        <Spinner color={Colors.COLOR_BASE_LIGHT} size={72}/>
                    </View>
                </Modal>

                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.state.modalHelp}
                    onRequestClose={() => {
                        this.setState({
                            modalHelp: false
                        });
                    }}
                    style={{elevation: 8}}
                >
                    <TouchableOpacity
                        onPress={() => {
                            this.setState({
                                modalHelp: false
                            });
                        }}
                        style={{flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#00000033'}}>
                        <View style={{width: width - 64, backgroundColor: Colors.COLOR_BASE_LIGHT, elevation: 4}}>
                            <Text
                                style={[styles.text, {
                                    backgroundColor: '#122638',
                                    color: Colors.COLOR_BASE_LIGHT,
                                    textAlign: 'center', padding: 8
                                }]}>
                                راهنمای گزارش سفر
                            </Text>

                            <View style={{padding: 24}}>
                                <Text style={styles.text}>
                                    در باکس ها جفت دکمه های شروع و پایان سفر اول از سمت چپ برای سفر از خانه به مدرسه و جفت دوم دکمه ها برای
                                    سفر بازگشت از مدرسه به خانه است.
                                </Text>
                                <Text style={styles.text}>
                                    دکمه اول سمت راست باکس برای اعلام غیبت دانش آموز توسط راننده می باشد.
                                </Text>
                                <Text style={styles.text}>
                                    دکمه پایین صفحه نمایش عملکردی مشابه دکمه های موجود در باکس ها دارد با این تفاوت که برای گزارش سفر همزمان
                                    کل دانش آموزان می باشد
                                </Text>
                            </View>

                            <View
                                style={{flexDirection: 'row'}}
                            >
                                <Button
                                    style={{width: '100%', borderRadius: 0, justifyContent: 'center', backgroundColor: '#09aaa2'}}
                                    onPress={() => {
                                        this.setState({
                                            modalHelp: false
                                        });
                                    }}
                                >
                                    <Text style={[styles.text, {textAlign: 'center', color: Colors.COLOR_BASE_LIGHT}]}>
                                        متوجه شدم
                                    </Text>
                                </Button>
                            </View>
                        </View>
                    </TouchableOpacity>
                </Modal>

                <Fab
                    active={this.state.fabActive}
                    direction={I18nManager.isRTL ? 'right' : 'left'}
                    style={{backgroundColor: this.state.fabActive ? '#29cac2' : '#09aaa2', elevation: 4}}
                    position={I18nManager.isRTL ? 'bottomLeft' : 'bottomRight'}
                    onPress={() => this.setState({fabActive: !this.state.fabActive})}>

                    {
                        this.state.fabSpinner ?
                            <Spinner/> :
                            <SimpleLineIcons name="people" color={Colors.COLOR_BASE_LIGHT} size={24}/>
                    }

                    <Button
                        style={{backgroundColor: 'red'}}
                        onPress={() => {
                            this.state.tripData.map((trip, i) => {
                                this.setState({
                                    fabSpinner: true,
                                    fabActive: false
                                });
                                this.asyncStore(trip, -1, 0, 3);
                            })
                        }}
                    >
                        <SimpleLineIcons name="user-unfollow" color={Colors.COLOR_BASE_LIGHT} size={18}/>
                    </Button>
                    <Button
                        style={{backgroundColor: '#009688'}}
                        onPress={() => {
                            this.state.tripData.map((trip, i) => {
                                this.setState({
                                    fabSpinner: true,
                                    fabActive: false
                                });
                                this.asyncStore(trip, 4, 2, 2);
                            })
                        }}
                    >
                        <SimpleLineIcons name="logout" color={Colors.COLOR_BASE_LIGHT} size={18}/>
                    </Button>
                    <Button
                        style={{backgroundColor: '#009688'}}
                        onPress={() => {
                            this.state.tripData.map((trip, i) => {
                                this.setState({
                                    fabSpinner: true,
                                    fabActive: false
                                });
                                this.asyncStore(trip, 3, 2, 1);
                            })
                        }}
                    >
                        <SimpleLineIcons name="login" color={Colors.COLOR_BASE_LIGHT} size={18}/>
                    </Button>
                    <Button
                        style={{backgroundColor: '#4CAF50'}}
                        onPress={() => {
                            this.state.tripData.map((trip, i) => {
                                this.setState({
                                    fabSpinner: true,
                                    fabActive: false
                                });
                                this.asyncStore(trip, 2, 1, 2);
                            })
                        }}
                    >
                        <SimpleLineIcons name="logout" color={Colors.COLOR_BASE_LIGHT} size={18}/>
                    </Button>
                    <Button
                        style={{backgroundColor: '#4CAF50'}}
                        onPress={() => {
                            this.state.tripData.map((trip, i) => {
                                this.setState({
                                    fabSpinner: true,
                                    fabActive: false
                                });
                                this.asyncStore(trip, 1, 1, 1);
                            })
                        }}
                    >
                        <SimpleLineIcons name="login" color={Colors.COLOR_BASE_LIGHT} size={18}/>
                    </Button>

                </Fab>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    header: {
        backgroundColor: '#122638'
    },
    tripBox: {
        flexDirection: 'column',
        width: '100%',
        marginBottom: 12,
        padding: 8,
        backgroundColor: '#fff',
        elevation: 2
    },
    itemHeader: {
        width: '100%',
        flexDirection: 'row-reverse',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: 8
    },
    itemControls: {
        width: '100%',
        height: 50,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 16
    },
    headerTitle: {
        fontFamily: 'sans',
        width: width - 200,
        direction: 'rtl',
        textAlign: 'right'
    },
    text: {
        fontSize: 14,
        fontFamily: 'sans',
    }
});
