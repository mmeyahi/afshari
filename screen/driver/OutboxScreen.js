import React from 'react';
import {Dimensions, Image, Modal, StyleSheet, TouchableOpacity} from 'react-native';

import {Body, Button, Container, Content, Header, Left, List, ListItem, Right, Spinner, Text, Thumbnail, Title, View} from 'native-base';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {API_MEDIA_AVATAR, Colors, HOST} from '../../config/Utility';
import dIcon from '../../assets/icons/d_outbox.png';
import trashIcon from '../../assets/icons/trash.png';
import BG from '../../assets/general/profile_bg.jpg';

const {width} = Dimensions.get('window');

export default class OutboxScreen extends React.Component {

    static navigationOptions = {
        drawerLabel: 'پیام  های ارسال شده',
        drawerIcon: ({tintColor}) => (
            <Image style={{height: 28, width: 28}} source={dIcon}/>
        )
    };

    constructor(props) {
        super(props);

        this.state = {
            outbox: [],
            selectedMsg: null,
            modalShowMsg: false,
            spinnerDelete: false,
            pageLoading: false
        }
    }

    componentDidMount() {
        this.fetchInboxData().then(respond => {
            if (respond) {
                this.setState({
                    outbox: respond.data.filter((msg) => {
                        return (msg.receiver_type != 3)
                    })
                })
            }

            this.setState({
                pageLoading: true
            })
        })
    }

    async fetchInboxData() {

        return await fetch(HOST + 'driver/massage', {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            credentials: 'same-origin',
        })
            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.status) {
                    return responseJson;
                }
                return false;
            }).catch((error) => {
                return false;
            });
    }

    async deleteMsg(smsg) {

        const msg = this.state.selectedMsg || smsg;

        return await fetch(HOST + 'driver/massage', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            credentials: 'same-origin',
            body: JSON.stringify({
                '_method': 'DELETE',
                id: msg.id,
                sender: msg.sender_id
            })
        })
            .then((response) => response.json())
            .then((responseJson) => {
                return msg;
            }).catch((error) => {
                return msg
            });
    }

    render() {
        return (
            <Container>
                <Image style={{flex: 1, position: 'absolute', top: 0, height: '100%', width: '100%'}} source={BG}/>
                <Header style={styles.header}>
                    <Left>
                        <Button
                            transparent
                            onPress={() => this.props.navigation.goBack()}
                        >
                            <Ionicons name="ios-arrow-back" size={32} color="#fff"/>
                        </Button>
                    </Left>
                    <Body>
                    <Title style={styles.headerTitle}>پیام های ارسال شده</Title>
                    </Body>
                    <Right>
                        <Button
                            transparent
                            onPress={() => this.props.navigation.toggleDrawer()}
                        >
                            <Ionicons name='ios-menu' size={32} color="#fff"/>
                        </Button>
                    </Right>
                </Header>

                {this.state.pageLoading ?
                    <Content style={{padding: 8}}>
                        <List>
                            {this.state.outbox.map((msg, i) => {
                                return (
                                    <ListItem
                                        key={i}
                                        style={{
                                            flexDirection: 'row-reverse', width: '100%', padding: 8,
                                            backgroundColor: msg.status === 1 ? '#09aaa2cc' : '#09aaa299',
                                            marginBottom: 4, borderRadius: 4
                                        }}
                                        onPress={() => {
                                            this.setState({
                                                selectedMsg: msg,
                                                modalShowMsg: true
                                            })
                                        }}
                                    >
                                        <View>
                                            <Thumbnail
                                                source={{uri: API_MEDIA_AVATAR + msg.receiver.avatar}}
                                                style={{borderWidth: 3, borderColor: '#fff'}}
                                            />
                                        </View>

                                        <View style={{padding: 8}}>
                                            <Text style={{color: Colors.COLOR_BASE_DARK, fontFamily: 'sans', fontSize: 14}}>
                                                {msg.receiver.fname + ' ' + msg.receiver.lname}
                                            </Text>
                                            <Text style={{color: Colors.COLOR_BASE_DARK, fontFamily: 'sans', fontSize: 12}}>
                                                {msg.receiver.mobile}
                                            </Text>
                                        </View>

                                        <Left>
                                            <Button
                                                transparent
                                                rounded
                                                style={{width: 32, height: 32}}
                                                onPress={() => {
                                                    this.setState({
                                                        spinnerDelete: i
                                                    });

                                                    this.deleteMsg(msg).then(res => {
                                                        this.setState({
                                                            spinnerDelete: false,
                                                            modalShowMsg: false,
                                                            outbox: this.state.outbox.filter((item) => {
                                                                return item.id !== res.id
                                                            })
                                                        });
                                                    })
                                                }}
                                            >
                                                {this.state.spinnerDelete === i ? <Spinner/> :
                                                    <Image style={{height: 28, width: 28}} source={trashIcon}/>
                                                }
                                            </Button>
                                        </Left>
                                    </ListItem>
                                )
                            })}
                        </List>
                    </Content>
                    :
                    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                        <Spinner/>
                    </View>
                }

                {!!this.state.selectedMsg &&
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.state.modalShowMsg}
                    onRequestClose={() => {
                        this.setState({
                            modalShowMsg: false
                        });
                    }}>
                    <TouchableOpacity
                        onPress={() => {
                            this.setState({
                                modalShowMsg: false
                            });
                        }}
                        style={{flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#00000033'}}>
                        <View style={{width: width - 64, backgroundColor: Colors.COLOR_BASE_LIGHT, elevation: 4}}>
                            <Text
                                style={[styles.text, {
                                    backgroundColor: '#122638',
                                    color: Colors.COLOR_BASE_LIGHT,
                                    textAlign: 'center',
                                    padding: 8
                                }]}>
                                {this.state.selectedMsg.receiver.fname + ' ' + this.state.selectedMsg.receiver.lname}
                            </Text>

                            <Text style={[styles.text, {padding: 16, minHeight: 120}]}>
                                {this.state.selectedMsg.msg}
                            </Text>

                            <View
                                style={{flexDirection: 'row'}}
                            >
                                <Button
                                    style={{width: '50%', borderRadius: 0, justifyContent: 'center', backgroundColor: '#122638'}}
                                    onPress={() => {
                                        this.setState({
                                            spinnerDelete: true
                                        });

                                        this.deleteMsg().then(res => {
                                            this.setState({
                                                spinnerDelete: false,
                                                modalShowMsg: false,
                                                outbox: this.state.outbox.filter((msg) => {
                                                    return msg.id !== res.id
                                                }),
                                                selectedMsg: null
                                            });
                                        })
                                    }}
                                >
                                    {this.state.modalSpinner ? <Spinner/> :
                                        <Text style={[styles.text, {textAlign: 'center', color: Colors.COLOR_BASE_LIGHT}]}>حذف</Text>
                                    }
                                </Button>
                                <Button
                                    style={{width: '50%', borderRadius: 0, justifyContent: 'center', backgroundColor: '#09aaa2'}}
                                    onPress={() => {
                                        this.setState({
                                            modalShowMsg: false
                                        });
                                    }}
                                >
                                    <Text style={[styles.text, {textAlign: 'center', color: Colors.COLOR_BASE_LIGHT}]}>
                                        تایید
                                    </Text>
                                </Button>
                            </View>
                        </View>
                    </TouchableOpacity>
                </Modal>
                }

            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.COLOR_BASE_LIGHT
    },
    header: {
        backgroundColor: '#122638'
    },
    headerTitle: {
        fontFamily: 'sans',
        width: width - 140,
        direction: 'rtl',
        textAlign: 'right'
    },
    card: {
        width: width - 16,
        alignSelf: 'center',
    },
    cardItemTop: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: 'rgba(200, 200, 200, 0.8)'
    },
    cardItem: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'flex-end',
        flexDirection: 'row',
        backgroundColor: 'rgba(200, 200, 200, 0.8)'
    },
    textTop: {
        fontSize: 14,
        fontFamily: 'sans',
        padding: 8,
        width: '100%'
    },
    text: {
        fontSize: 14,
        fontFamily: 'sans',
    }
});
