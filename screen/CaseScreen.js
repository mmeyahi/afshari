import React from 'react';
import {AsyncStorage, Image, Modal, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import BG from '../assets/general/bg.jpg';
import pIcon from '../assets/general/opt_parent.png';
import dIcon from '../assets/general/opt_driver.png';

import {Colors} from "../config/Utility";

class CaseScreen extends React.Component {

    static navigationOptions = {
        headerStyle: {
            display: 'none'
        }
    };

    constructor(props) {
        super(props);

        this.state = {
            modalVisible: false,
            modalTitle: '',
            modalMsg: '',
            modalIcon: 'alert-triangle'
        }
    }

    async fetchUserData() {
        return await AsyncStorage.getItem('@SchoolService:userData');
    }

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.container}>
                <Image style={{flex: 1, position: 'absolute', top: 0, height: '100%', width: '100%'}} source={BG}/>

                <TouchableOpacity
                    onPress={() => {
                        this.fetchUserData()
                            .then(data => {
                                const userData = JSON.parse(data);
                                if (userData.students.length) {
                                    this.props.navigation.navigate('parent');
                                } else {
                                    this.setState({
                                        modalVisible: true,
                                        modalTitle: 'دسترسی ورود ندارید',
                                        modalMsg: 'با مراجعه به مدرسه هر یک از فرزندان خود این گزینه را فعال کنید',
                                    });
                                }
                            })
                    }}
                    style={styles.parent_option}>
                    <View>
                        <Image style={{width: 240, height: 125, alignSelf: 'center'}} source={pIcon}/>
                        <Text style={[styles.label, {color: Colors.COLOR_BASE_LIGHT}]}>ورود بعنوان ولی دانش آموز</Text>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity
                    onPress={() => {
                        this.fetchUserData()
                            .then(data => {

                                const userData = JSON.parse(data);

                                if (!!userData.driver.id) {
                                    this.props.navigation.navigate('driver');
                                } else {
                                    this.setState({
                                        modalVisible: true,
                                        modalTitle: 'دسترسی ورود ندارید',
                                        modalMsg: 'با مراجعه به یک مدرسه و ثبت نام بعنوان سرویس مدرسه این گزینه برای شما فعال می شود',
                                    });
                                }
                            })
                    }}
                    style={styles.driver_option}>
                    <View>
                        <Image style={{width: 200, height: 125, alignSelf: 'center'}} source={dIcon}/>
                        <Text style={[styles.label, {color: Colors.COLOR_BASE_LIGHT}]}>ورود بعنوان سرویس مدرسه</Text>
                    </View>
                </TouchableOpacity>

                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.state.modalVisible}
                    onRequestClose={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }}>
                    <TouchableOpacity
                        onPress={() =>
                            this.setModalVisible(!this.state.modalVisible)
                        }
                        style={styles.modal}
                    >
                        <View style={{backgroundColor: Colors.COLOR_BASE_LIGHT2}}>
                            <Feather style={styles.icon} name={this.state.modalIcon} size={28}
                                     color={Colors.COLOR_BASE_LIGHT}/>
                        </View>
                        <View style={{padding: 8}}>
                            <Text style={{color: Colors.COLOR_BASE_DARK, fontFamily: 'sans', fontSize: 12}}>
                                {this.state.modalTitle}
                            </Text>
                            <Text style={{color: Colors.COLOR_BASE_DARK, fontFamily: 'sans', fontSize: 7}}>
                                {this.state.modalMsg}
                            </Text>
                        </View>
                    </TouchableOpacity>
                </Modal>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.COLOR_BASE_LIGHT2
    },
    parent_option: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        width: 300,
        height: 200,
        backgroundColor: '#09aaa2cc',
        margin: 16,
        borderRadius: 20,
        elevation: 0,
    },
    driver_option: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        width: 300,
        height: 200,
        backgroundColor: '#122638cc',
        margin: 16,
        borderRadius: 20,
        elevation: 0,
    },
    icon: {
        alignSelf: 'center',
        margin: 16
    },
    label: {
        textAlign: 'center',
        fontSize: 18,
        fontFamily: 'sans',
    },
    modal: {
        flexDirection: 'row-reverse',
        alignItems: 'center',
        backgroundColor: Colors.COLOR_BASE_LIGHT,
        margin: 8,
        elevation: 2
    }
});

export default CaseScreen;
