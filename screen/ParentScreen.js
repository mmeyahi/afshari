import React from 'react';
import {Alert, AsyncStorage, Dimensions, I18nManager, Modal, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {Body, Button, Container, Fab, Header, Left, List, ListItem, Right, Spinner, Thumbnail, Title} from 'native-base';

import GPSState from 'react-native-gps-state';

import WebViewLeaflet from 'react-native-webview-leaflet';
import Icon from 'react-native-vector-icons/SimpleLineIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import * as svgIcons from '../assets/general/marker_32_1';
import {API_MEDIA_AVATAR, Colors, HOST} from '../config/Utility';

// const isAndroid = Platform.OS === 'android';
const {width} = Dimensions.get('window');
let mapListener = null;

export default class ParentScreen extends React.Component {

    static navigationOptions = {
        drawerLabel: 'مشاهده نقشه',
        drawerIcon: ({tintColor}) => (
            <Icon name="map" color={tintColor} style={{marginLeft: 16, width: 36, height: 32}} size={22}/>
        )
    };

    constructor(props) {
        super(props);

        this.state = {
            userData: null,
            selecteddriver: null,
            driverModal: false,
            studentLocationModal: false,
            studentSelected: [],
            studentPosition: null,
            positionWait: false,
            locationModal: false
        }
    }

    componentWillMount() {
        this.fetchUserData()
            .then(data => {
                const userData = JSON.parse(data);
                this.setState({userData})
            });

        GPSState.getStatus().then((status) => {
            if (status === 1) {
                this.setState({
                    locationModal: true
                })
            }
        });
    }

    componentDidMount = () => {
        mapListener = setInterval(() => this.mapProsses(), 7000);
    };

    componentWillUnmount() {
        clearInterval(mapListener);
    }

    async fetchUserData() {
        return await AsyncStorage.getItem('@SchoolService:userData');
    }

    async fetchMapData() {

        return await fetch(HOST + 'parent', {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            credentials: 'same-origin',
        })
            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.status) {
                    return responseJson;
                }
                return false;
            });
    }

    async storStudentPosition() {
        this.setState({
            positionWait: true
        });

        return await fetch(HOST + 'parent/position', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            credentials: 'same-origin',
            body: JSON.stringify({
                students: this.state.studentSelected,
                position: this.state.studentPosition
            }),
        })
            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.status) {
                    return responseJson;
                }
                return false;
            });
    }

    mapProsses() {
        this.fetchMapData().then(response => {

            const mapData = [];
            response.data.map((dat, i) => {
                if (dat.driver.trips.status && dat.driver.trips.status == 1 || true) {//TODO remove this line...
                    mapData.push({
                        id: dat.id,
                        coords: [dat.driver.lat, dat.driver.lng],
                        icon: svgIcons.car_marker,
                        size: [32, 32]
                    });
                }
            });

            this.webViewLeaflet.sendMessage({
                zoom: 12,
                locations: mapData
            });
        })
    }

    onMapClicked = ({payload}) => {
        this.setState({
            studentLocationModal: true,
            studentPosition: [payload.coords.latitude, payload.coords.longitude]
        })
    };

    onMapMarkerClicked = ({payload}) => {
        let driver = null;
        this.state.userData.students.map((std, i) => {
            if (std.id == payload.id) {
                driver = std.driver;
            }
        });

        this.setState({
            selecteddriver: driver,
            driverModal: true
        })
    };

    onLoad = (event) => {
        this.mapProsses();
        navigator.geolocation.getCurrentPosition((location) => {
            this.webViewLeaflet.sendMessage({
                zoom: 12,
                centerPosition: [location.coords.latitude, location.coords.longitude]
            });
        });
    };

    isInclude(list, value) {
        let result = false;
        for (let i = 0; i < list.length; i++) {
            if (list[i] == value) {
                result = true;
                break;
            }
        }
        return result;
    }

    render() {
        return (
            /*
            <WebView
                source={{
                uri: isAndroid ? 'file:///android_asset/index.html'
                    : './external/index.html'
            }}
                style={styles.container}
            />
            */
            <Container>
                <Header style={styles.header}>
                    <Left>
                        <Button
                            transparent
                            onPress={() => this.props.navigation.navigate('parentMenuScreen')}
                        >
                            <Ionicons name="ios-arrow-back" size={32} color="#fff"/>
                        </Button>
                    </Left>
                    <Body>
                    <Title style={styles.headerTitle}>رانندگان در نقشه</Title>
                    </Body>
                    <Right>
                        <Button
                            transparent
                            onPress={() => this.props.navigation.toggleDrawer()}
                        >
                            <Ionicons name='ios-menu' size={32} color="#fff"/>
                        </Button>
                    </Right>
                </Header>

                <WebViewLeaflet
                    ref={(component) => (this.webViewLeaflet = component)}
                    centerButton={false}
                    eventReceiver={this}
                />

                {!!this.state.userData &&
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.state.studentLocationModal}
                    onRequestClose={() => {
                        this.setState({
                            studentLocationModal: false
                        });
                    }}
                    style={{elevation: 8}}
                >
                    <TouchableOpacity
                        onPress={() => {
                            this.setState({
                                studentLocationModal: false
                            });
                        }}
                        style={{flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#00000033'}}>
                        <View style={{width: width - 64, backgroundColor: Colors.COLOR_BASE_LIGHT, elevation: 4}}>
                            <Text
                                style={[styles.text, {
                                    backgroundColor: '#122638',
                                    color: Colors.COLOR_BASE_LIGHT,
                                    textAlign: 'center'
                                }]}>
                                تنظیم مختصات جغرافیایی دانش آموز
                            </Text>
                            <List>
                                {this.state.userData.students.map((student, i) => {
                                    return (
                                        <ListItem
                                            key={i}
                                            style={{flexDirection: 'row-reverse'}}
                                            onPress={() => {
                                                if (this.isInclude(this.state.studentSelected, student.id)) {
                                                    this.setState({
                                                        studentSelected: this.state.studentSelected.filter((item) => {
                                                            return (item != student.id);
                                                        })
                                                    })
                                                } else {
                                                    let list = this.state.studentSelected;
                                                    list.push(student.id);
                                                    this.setState({
                                                        studentSelected: list
                                                    })
                                                }
                                            }}
                                        >
                                            <View>
                                                <Thumbnail
                                                    source={{uri: API_MEDIA_AVATAR + student.avatar}}
                                                    style={{borderWidth: 3, borderColor: Colors.COLOR_BASE_LIGHT2}}
                                                />
                                            </View>

                                            <View style={{padding: 8}}>
                                                <Text style={{color: Colors.COLOR_BASE_DARK, fontFamily: 'sans', fontSize: 12}}>
                                                    {student.name}
                                                </Text>
                                                <Text style={{color: Colors.COLOR_BASE_DARK, fontFamily: 'sans', fontSize: 7}}>
                                                    نام راننده :
                                                    {student.driver.user.fname + ' ' + student.driver.user.lname}
                                                </Text>
                                            </View>

                                            {this.isInclude(this.state.studentSelected, student.id) &&
                                            < Left>
                                                <Ionicons name="ios-checkmark" color='green' style={{marginLeft: 8}} size={32}/>
                                            </Left>
                                            }
                                        </ListItem>
                                    )
                                })}
                            </List>
                            <View
                                style={{flexDirection: 'row'}}
                            >
                                <Button
                                    style={{width: '50%', borderRadius: 0, backgroundColor: '#122638'}}
                                    onPress={() => {
                                        this.setState({
                                            studentLocationModal: false
                                        });
                                    }}
                                >
                                    <Text style={[styles.text, {textAlign: 'center', color: Colors.COLOR_BASE_LIGHT}]}>انصراف</Text>
                                </Button>
                                <Button
                                    style={{width: '50%', borderRadius: 0, justifyContent: 'center', backgroundColor: '#09aaa2'}}
                                    onPress={() => {
                                        this.storStudentPosition().then(res => {
                                            if (res.status) {
                                                Alert.alert('وضعیت بروزرسانی', 'موقعیت جدید با موفقیت ثبت شد. برای مشاهده تغییرات خارج و مجددا وارد اپ شوید.');
                                            }
                                            this.setState({
                                                positionWait: false,
                                                studentLocationModal: false
                                            });
                                        })
                                    }}
                                >
                                    {this.state.positionWait ? <Spinner/> :
                                        <Text style={[styles.text, {textAlign: 'center', color: Colors.COLOR_BASE_LIGHT}]}>
                                            اعمال
                                        </Text>
                                    }
                                </Button>
                            </View>
                        </View>
                    </TouchableOpacity>
                </Modal>
                }

                {!!this.state.selecteddriver &&
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.state.driverModal}
                    onRequestClose={() => {
                        this.setState({
                            driverModal: false
                        });
                    }}>
                    <TouchableOpacity
                        onPress={() =>
                            this.setState({
                                driverModal: false
                            })
                        }
                        style={styles.modal}
                    >
                        <View style={{backgroundColor: Colors.COLOR_BASE_LIGHT2}}>
                            <Thumbnail
                                source={{uri: API_MEDIA_AVATAR + this.state.selecteddriver.user.avatar}}
                                style={{borderWidth: 3, borderColor: Colors.COLOR_BASE_LIGHT}}
                            />
                        </View>

                        <View style={{padding: 8}}>
                            <Text style={{color: Colors.COLOR_BASE_DARK, fontFamily: 'sans', fontSize: 12}}>
                                {this.state.selecteddriver.user.fname + ' ' + this.state.selecteddriver.user.lname}
                            </Text>
                            <Text style={{color: Colors.COLOR_BASE_DARK, fontFamily: 'sans', fontSize: 7}}>
                                {this.state.selecteddriver.user.mobile}
                            </Text>
                        </View>

                        <Left>
                            <Icon name="close" color={Colors.COLOR_BASE_DARK2} style={{marginLeft: 16}} size={32}/>
                        </Left>
                    </TouchableOpacity>
                </Modal>
                }

                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.state.locationModal}
                    onRequestClose={() => {
                        this.setState({
                            locationModal: false
                        });
                    }}
                    style={{elevation: 8}}
                >
                    <TouchableOpacity
                        onPress={() => {
                            this.setState({
                                locationModal: false
                            });
                        }}
                        style={{flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#00000033'}}>
                        <View style={{width: width - 64, backgroundColor: Colors.COLOR_BASE_LIGHT, elevation: 4}}>
                            <Text
                                style={[styles.text, {
                                    backgroundColor: '#122638',
                                    color: Colors.COLOR_BASE_LIGHT,
                                    textAlign: 'center'
                                }]}>
                                مکان یاب دستگاه فعال نیست
                            </Text>

                            <View style={{padding: 24}}>
                                <Text style={styles.text}>
                                    برای استفاده از امکانات نقشه باید مکان یاب دستگاه فعال باشد. اکنون فعال شود؟
                                </Text>
                            </View>

                            <View
                                style={{flexDirection: 'row'}}
                            >
                                <Button
                                    style={{width: '50%', borderRadius: 0, backgroundColor: '#122638'}}
                                    onPress={() => {
                                        this.setState({
                                            locationModal: false
                                        });
                                    }}
                                >
                                    <Text style={[styles.text, {textAlign: 'center', color: Colors.COLOR_BASE_LIGHT}]}>خیر</Text>
                                </Button>
                                <Button
                                    style={{width: '50%', borderRadius: 0, justifyContent: 'center', backgroundColor: '#09aaa2'}}
                                    onPress={() => {
                                        this.setState({
                                            locationModal: false
                                        });

                                        GPSState.openSettings();

                                    }}
                                >
                                    <Text style={[styles.text, {textAlign: 'center', color: Colors.COLOR_BASE_LIGHT}]}>
                                        بله
                                    </Text>
                                </Button>
                            </View>
                        </View>
                    </TouchableOpacity>
                </Modal>

                <Fab
                    direction={I18nManager.isRTL ? 'right' : 'left'}
                    style={{backgroundColor: '#09aaa2'}}
                    position={I18nManager.isRTL ? 'bottomLeft' : 'bottomRight'}
                    onPress={() => {
                        navigator.geolocation.getCurrentPosition((location) => {
                            this.webViewLeaflet.sendMessage({
                                centerPosition: [location.coords.latitude + 0.0001, location.coords.longitude + 0.0001]
                            });

                            this.webViewLeaflet.sendMessage({
                                centerPosition: [location.coords.latitude, location.coords.longitude]
                            });
                        });
                    }}>
                    <Ionicons name='md-locate' size={48} color={Colors.COLOR_BASE_DARK} style={{margin: 16}}/>
                </Fab>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.COLOR_BASE_LIGHT
    },
    header: {
        backgroundColor: '#122638'
    },
    headerTitle: {
        fontFamily: 'sans',
        width: width - 140,
        direction: 'rtl',
        textAlign: 'right'
    },
    card: {
        width: width - 16,
        alignSelf: 'center',
    },
    cardItemTop: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: 'rgba(200, 200, 200, 0.8)'
    },
    cardItem: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'flex-end',
        flexDirection: 'row',
        backgroundColor: 'rgba(200, 200, 200, 0.8)'
    },
    textTop: {
        fontSize: 14,
        fontFamily: 'sans',
        padding: 8,
        width: '100%'
    },
    text: {
        fontSize: 14,
        fontFamily: 'sans',
        padding: 16,
        width: '100%'
    },
    modal: {
        flexDirection: 'row-reverse',
        alignItems: 'center',
        backgroundColor: Colors.COLOR_BASE_LIGHT,
        margin: 8,
        elevation: 2,
        marginTop: 70,
    }
});
