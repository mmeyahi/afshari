import React from 'react';
import {AsyncStorage, Dimensions, Image, StyleSheet, TouchableHighlight} from 'react-native';

import {
    Body,
    Button,
    Card,
    CardItem,
    Container,
    Content,
    Header,
    Left,
    List,
    ListItem,
    Right,
    Spinner,
    Text,
    Thumbnail,
    Title,
    View
} from 'native-base';
import moment from 'jalali-moment';

import GridList from 'react-native-grid-list';

import Ionicons from 'react-native-vector-icons/Ionicons';
import {API_MEDIA_AVATAR, Colors, HOST} from '../../config/Utility';
import dIcon from '../../assets/icons/d_calendar.png';
import BG from '../../assets/general/profile_bg.jpg';

const {width} = Dimensions.get('window');

export default class CalendarScreen extends React.Component {

    static navigationOptions = {
        drawerLabel: 'اعلام غیبت',
        drawerIcon: ({tintColor}) => (
            <Image style={{height: 28, width: 28, tintColor}} source={dIcon}/>
        )
    };

    constructor(props) {
        super(props);

        this.state = {
            students: [],
            calData: [],
            selected: null,
            spinner: -1,
            selectSpinner: false
        }
    }

    componentWillMount() {
        this.fetchUserData()
            .then(data => {
                const userData = JSON.parse(data);
                this.setState({
                    students: userData.students
                });

                if (userData.students.length) {
                    const data = [];
                    const calendars = userData.students[0].calendars;

                    for (let i = 0; i < 30; i++) {
                        const dt = moment().add(i, 'day');
                        data.push({
                            index: i,
                            dayName: dt.locale('fa').format('dddd'),
                            date: dt.locale('fa').format('YYYY/MM/DD'),
                            off: this.isInclude(calendars, dt.locale('fa').format('YYYY/MM/DD')),
                            gdate: dt.locale('en').format('YYYY/MM/DD')
                        });
                    }

                    this.setState({
                        selected: userData.students[0],
                        calData: data
                    })
                }
            });
    }

    async fetchUserData() {
        return await AsyncStorage.getItem('@SchoolService:userData');
    }

    isInclude(list, value) {
        let result = false;
        for (let i = 0; i < list.length; i++) {
            const cal = moment(list[i].offDate, 'YYYY-MM-DD').locale('fa').format('YYYY/MM/DD');

            if (cal === value) {
                result = true;
                break;
            }
        }
        return result;
    }

    async addOffDate(offDate) {
        this.setState({
            sending: true
        });

        return await fetch(HOST + 'parent/off', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            credentials: 'same-origin',
            body: JSON.stringify({
                student_id: this.state.selected.id,
                offDate,
                type: 1,
                desc: '...'
            }),
        })
            .then((response) => response.json())
            .then((responseJson) => {
                return responseJson;
            });
    }

    renderItem = ({item, index}) => (
        <TouchableHighlight
            underlayColor='transparent'
            style={{width: '100%', height: '100%'}}
            onPress={() => {
                if (this.state.spinner === -1 && item.dayName != 'جمعه') {
                    this.setState({spinner: index});
                    this.addOffDate(item.gdate).then(res => {
                        if (res.status) {
                            const updateCal = [];

                            this.state.calData.map((cal, i) => {
                                const tmp = cal;
                                if (item.index === i) {
                                    tmp.off = !tmp.off;
                                }
                                updateCal.push(tmp);
                            });

                            this.setState({
                                spinner: -1,
                                calData: updateCal,
                                students: res.data
                            })
                        }
                    })
                }
            }}
        >
            <Card>
                <CardItem
                    style={{backgroundColor: (item.off || item.dayName == 'جمعه') ? '#09aaa2' : '#09aaa255'}}>

                    <Body style={{justifyContent: 'center', alignItems: 'center'}}>
                    {index === this.state.spinner ?
                        <Spinner style={{width: 48, height: 48}} size={48}/> :
                        <View>
                            <Text style={{
                                fontSize: 12, textAlign: 'center', fontFamily: 'sans',
                                color: (item.off || item.dayName == 'جمعه') ? '#fff' : Colors.COLOR_BASE_DARK
                            }}>
                                {item.dayName}
                            </Text>
                            <Text style={{
                                fontSize: 12, textAlign: 'center', fontFamily: 'sans',
                                color: (item.off || item.dayName == 'جمعه') ? Colors.COLOR_BASE_LIGHT : Colors.COLOR_BASE_DARK
                            }}>{item.date}</Text>
                        </View>
                    }
                    </Body>

                </CardItem>
            </Card>
        </TouchableHighlight>
    );

    render() {
        return (
            <Container>
                <Image style={{flex: 1, position: 'absolute', top: 0, height: '100%', width: '100%'}} source={BG}/>
                <Header style={styles.header}>
                    <Left>
                        <Button
                            transparent
                            onPress={() => this.props.navigation.goBack()}
                        >
                            <Ionicons name="ios-arrow-back" size={32} color="#fff"/>
                        </Button>
                    </Left>
                    <Body>
                    <Title style={styles.headerTitle}>اعلام غیبت</Title>
                    </Body>
                    <Right>
                        <Button
                            transparent
                            onPress={() => this.props.navigation.toggleDrawer()}
                        >
                            <Ionicons name='ios-menu' size={32} color="#fff"/>
                        </Button>
                    </Right>
                </Header>
                <Content style={{padding: 8}}>
                    <List>
                        {this.state.students.map((student, i) => {
                            return (
                                <ListItem
                                    key={i}
                                    style={{
                                        flexDirection: 'row-reverse', width: '100%', padding: 8,
                                        backgroundColor: '#09aaa2cc', marginBottom: 4, borderRadius: 4
                                    }}
                                    onPress={() => {
                                        const data = [];
                                        const calendars = student.calendars;

                                        for (let i = 0; i < 30; i++) {
                                            const dt = moment().add(i, 'day');
                                            data.push({
                                                index: i,
                                                dayName: dt.locale('fa').format('dddd'),
                                                date: dt.locale('fa').format('YYYY/MM/DD'),
                                                off: this.isInclude(calendars, dt.locale('fa').format('YYYY/MM/DD')),
                                                gdate: dt.locale('en').format('YYYY/MM/DD')
                                            });
                                        }

                                        this.setState({
                                            selected: student,
                                            calData: data
                                        })
                                    }}
                                >
                                    <View>
                                        <Thumbnail
                                            source={{uri: API_MEDIA_AVATAR + student.avatar}}
                                            style={{borderWidth: 3, borderColor: '#fff', backgroundColor: '#09ccc2'}}
                                        />
                                    </View>

                                    <View style={{padding: 8}}>
                                        <Text style={{color: Colors.COLOR_BASE_DARK, fontFamily: 'sans', fontSize: 14}}>
                                            {student.name}
                                        </Text>
                                        <Text style={{color: Colors.COLOR_BASE_DARK, fontFamily: 'sans', fontSize: 12}}>
                                            نام راننده:
                                            {student.driver.user.fname + ' ' + student.driver.user.lname}
                                        </Text>
                                    </View>

                                    {(!!this.state.selected && this.state.selected.id === student.id) &&
                                    < Left>
                                        <Ionicons name="ios-checkmark" color='#fff' style={{marginLeft: 8}} size={48}/>
                                    </Left>
                                    }
                                </ListItem>
                            )
                        })}
                    </List>

                    <GridList
                        showSeparator={false}
                        data={this.state.calData}
                        separatorBorderWidth={1}
                        separatorBorderColor="silver"
                        numColumns={3}
                        renderItem={this.renderItem}
                        itemStyle={{height: 70}}
                        style={{marginBottom: 20}}
                    />
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    header: {
        backgroundColor: '#122638'
    },
    headerTitle: {
        fontFamily: 'sans',
        width: width - 140,
        direction: 'rtl',
        textAlign: 'right'
    }
});
