import React from 'react';
import {Alert, AsyncStorage, Dimensions, I18nManager, Image, StyleSheet} from 'react-native';

import {
    Body,
    Button,
    Container,
    Content,
    Fab,
    Form,
    Header,
    Left,
    List,
    ListItem,
    Right,
    Spinner,
    Text,
    Textarea,
    Thumbnail,
    Title,
    View
} from 'native-base';

import Ionicons from 'react-native-vector-icons/Ionicons';
import {API_MEDIA_AVATAR, Colors, HOST} from '../../config/Utility';
import BG from '../../assets/general/bg.jpg';
import dIcon from '../../assets/icons/d_send_msg.png';
import sendIcon from '../../assets/icons/send.png';

const {width} = Dimensions.get('window');

export default class SendMessageScreen extends React.Component {

    static navigationOptions = {
        drawerLabel: 'ارسال پیام',
        drawerIcon: ({tintColor}) => (
            <Image style={{height: 28, width: 28, tintColor}} source={dIcon}/>
        )
    };

    constructor(props) {
        super(props);

        this.state = {
            msg: '',
            reciverList: [],
            selected: [],
            sending: false
        }
    }

    componentWillMount() {
        this.fetchUserData()
            .then(data => {
                const userData = JSON.parse(data);

                let reciverList = [];
                userData.students.map((student, i) => {

                    if (!this.isInclude(reciverList, student.driver.id)) {
                        let item = student.driver;
                        item.type = 3;
                        reciverList.push(item);
                    }

                    if (!this.isInclude(reciverList, student.school.id)) {
                        let item = student.school;
                        item.type = 1;
                        reciverList.push(item);
                    }
                });

                this.setState({reciverList});
            })
    }

    async fetchUserData() {
        return await AsyncStorage.getItem('@SchoolService:userData');
    }

    isInclude(list, value) {
        let result = false;
        for (let i = 0; i < list.length; i++) {
            if (list[i].id == value) {
                result = true;
                break;
            }
        }
        return result;
    }

    async sendMsg(reciver) {
        this.setState({
            sending: true
        });

        return await fetch(HOST + 'parent/massage', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            credentials: 'same-origin',
            body: JSON.stringify({
                rid: reciver.id,
                msg: this.state.msg,
                type: 1,
                rtype: reciver.type
            }),
        })
            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.status) {
                    return responseJson;
                }
                return false;
            });
    }

    render() {
        return (
            <Container>
                <Image style={{flex: 1, position: 'absolute', top: 0, height: '100%', width: '100%'}} source={BG}/>
                <Header style={styles.header}>
                    <Left>
                        <Button
                            transparent
                            onPress={() => this.props.navigation.goBack()}
                        >
                            <Ionicons name="ios-arrow-back" size={32} color="#fff"/>
                        </Button>
                    </Left>
                    <Body>
                    <Title style={styles.headerTitle}>ارسال پیام</Title>
                    </Body>
                    <Right>
                        <Button
                            transparent
                            onPress={() => this.props.navigation.toggleDrawer()}
                        >
                            <Ionicons name='ios-menu' size={32} color="#fff"/>
                        </Button>
                    </Right>
                </Header>
                <Content style={{padding: 8}}>
                    <List>
                        {this.state.reciverList.map((reciver, i) => {
                            return (
                                <ListItem
                                    key={i}
                                    style={{
                                        flexDirection: 'row-reverse', width: '100%', padding: 8,
                                        backgroundColor: '#09aaa2cc', marginBottom: 4, borderRadius: 4
                                    }}
                                    onPress={() => {
                                        if (this.isInclude(this.state.selected, reciver.id)) {
                                            this.setState({
                                                selected: this.state.selected.filter((item) => {
                                                    return (item.id != reciver.id);
                                                })
                                            })
                                        } else {
                                            let list = this.state.selected;
                                            list.push(reciver);
                                            this.setState({
                                                selected: list
                                            })
                                        }
                                    }}
                                >
                                    <View>
                                        <Thumbnail
                                            source={{uri: API_MEDIA_AVATAR + reciver.user.avatar}}
                                            style={{borderWidth: 3, borderColor: '#fff'}}
                                        />
                                    </View>

                                    <View style={{padding: 8}}>
                                        <Text style={{color: Colors.COLOR_BASE_DARK, fontFamily: 'sans', fontSize: 12}}>
                                            {reciver.user.fname + ' ' + reciver.user.lname}
                                        </Text>
                                        <Text style={{color: Colors.COLOR_BASE_DARK, fontFamily: 'sans', fontSize: 10}}>
                                            {reciver.type == 1 ? 'مدیر مدرسه ' : 'راننده '}
                                            {reciver.user.mobile}
                                        </Text>
                                    </View>

                                    {this.isInclude(this.state.selected, reciver.id) &&
                                    < Left>
                                        <Ionicons name="ios-checkmark" color='#fff' style={{marginLeft: 8}} size={48}/>
                                    </Left>
                                    }
                                </ListItem>
                            )
                        })}
                    </List>

                    <Form style={{elevation: 8}}>
                        <Textarea
                            rowSpan={6}
                            style={[styles.text, {borderRadius: 4, padding: 16, backgroundColor: '#ffffffcc'}]}
                            bordered
                            placeholder='متن پیام را وارد کنید'
                            onChangeText={(msg) => this.setState({msg})}
                        />
                    </Form>
                </Content>

                <Fab
                    direction={I18nManager.isRTL ? 'right' : 'left'}
                    style={{backgroundColor: '#09aaa2'}}
                    position={I18nManager.isRTL ? 'bottomLeft' : 'bottomRight'}
                    onPress={() => {
                        const length = this.state.selected.length - 1;
                        if (length >= 0) {
                            if (this.state.msg.length !== 0 && this.state.msg !== '') {
                                this.state.selected.map((reciver, i) => {
                                    this.sendMsg(reciver).then(res => {
                                        if (i === length) {
                                            if (res.status) {
                                                Alert.alert('وضعیت ارسال پیام', 'پیام با موفقیت به گیرنده ها ارسال شد');
                                            } else {
                                                Alert.alert('وضعیت ارسال پیام', 'پیام ارسال نشد');
                                            }
                                            this.setState({sending: false});
                                        }
                                    })
                                })
                            } else {
                                Alert.alert('وضعیت ارسال پیام', 'ورود متن پیام الزامی است');
                            }
                        } else {
                            Alert.alert('وضعیت ارسال پیام', 'انتخاب حداقل یک گیرنده الزامی است');
                        }
                    }}>
                    {this.state.sending ?
                        <Spinner color={Colors.COLOR_BASE_LIGHT}/> :
                        <Image style={{height: 28, width: 28}} source={sendIcon}/>
                    }
                </Fab>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    header: {
        backgroundColor: '#122638'
    },
    headerTitle: {
        fontFamily: 'sans',
        width: width - 140,
        direction: 'rtl',
        textAlign: 'right'
    },
    text: {
        fontSize: 14,
        fontFamily: 'sans',
    }
});
