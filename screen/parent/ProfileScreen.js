import React from 'react';
import {AsyncStorage, Dimensions, Image, Modal, Alert, StyleSheet, TouchableOpacity} from 'react-native';

import {
    Body,
    Button,
    Card,
    CardItem,
    Container,
    Content,
    Form,
    Header,
    Input,
    Item,
    Label,
    Left,
    Right,
    Spinner,
    Text,
    Thumbnail,
    Title,
    View
} from 'native-base';

import ImagePicker from 'react-native-image-crop-picker';

import Ionicons from 'react-native-vector-icons/Ionicons';
import {API_MEDIA_AVATAR, Colors, HOST} from '../../config/Utility';
import BG from '../../assets/general/profile_bg.jpg';
import camera from '../../assets/icons/camera.png';
import galery from '../../assets/icons/galery.png';
import dIcon from '../../assets/icons/d_profile.png';
import saveIcon from '../../assets/icons/save.png';

const {width} = Dimensions.get('window');

export default class ProfileScreen extends React.Component {

    static navigationOptions = {
        drawerLabel: 'پروفایل',
        drawerIcon: ({tintColor}) => (
            <Image style={{height: 28, width: 28, tintColor}} source={dIcon}/>
        )
    };

    constructor(props) {
        super(props);

        this.state = {
            userData: {},
            sending: false,
            newAvatar: null,
            newFName: '',
            newLName: '',
            canUpdate: false,
            modalVisible: false
        }
    }

    componentWillMount() {
        this.fetchUserData()
            .then(data => {
                const dat = JSON.parse(data);

                const userData = {
                    id: dat.id,
                    avatar: dat.user.avatar,
                    fname: dat.user.fname,
                    lname: dat.user.lname,
                    meliCode: dat.user.meliCode,
                    mobile: dat.user.mobile
                };

                this.setState({userData});
            })
    }

    async fetchUserData() {
        return await AsyncStorage.getItem('@SchoolService:userData');
    }

    async updateProfile() {
        this.setState({
            sending: true
        });

        const data = {};
        data._method = 'PATCH';

        if (!!this.state.newAvatar) {
            data.avatar = this.state.newAvatar;
        }
        if (!!this.state.newFName) data.fname = this.state.newFName;
        if (!!this.state.newLName) data.lname = this.state.newLName;

        return await fetch(HOST + 'parent/profile', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            credentials: 'same-origin',
            body: JSON.stringify(data),
        })
            .then((response) => response.json())
            .then((responseJson) => {
                return responseJson;
            });
    }

    render() {
        return (
            <Container>
                <Image style={{flex: 1, position: 'absolute', top: 0, height: '100%', width: '100%'}} source={BG}/>
                <Header style={styles.header}>
                    <Left>
                        <Button
                            transparent
                            onPress={() => this.props.navigation.goBack()}
                        >
                            <Ionicons name="ios-arrow-back" size={32} color="#fff"/>
                        </Button>
                    </Left>
                    <Body>
                    <Title style={styles.headerTitle}>مشخصات کاربری</Title>
                    </Body>
                    <Right>
                        <Button
                            transparent
                            onPress={() => this.props.navigation.toggleDrawer()}
                        >
                            <Ionicons name='ios-menu' size={32} color="#fff"/>
                        </Button>
                    </Right>
                </Header>

                <Content style={{padding: 8}}>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>

                        <View style={{padding: 4, margin: 4, elevation: 8, borderRadius: 35}}>
                            <Button
                                rounded
                                style={{
                                    width: 70, height: 70, justifyContent: 'space-around',
                                    alignItems: 'center', backgroundColor: '#122638'
                                }}
                                onPress={() => {
                                    ImagePicker.openCamera({
                                        width: 200,
                                        height: 200,
                                        cropping: true,
                                        includeBase64: true,
                                        cropperToolbarTitle: 'برش تصویر'
                                    }).then(newAvatar => {
                                        this.setState({
                                            newAvatar
                                        })
                                    });
                                }}
                            >
                                <Image style={{height: 60, width: 60}} source={camera}/>
                            </Button>
                        </View>

                        <View style={{elevation: 8, borderRadius: 70}}>
                            <Thumbnail
                                source={this.state.newAvatar ? {uri: this.state.newAvatar.path} : {uri: API_MEDIA_AVATAR + this.state.userData.avatar}}
                                style={styles.avatar}
                            />
                        </View>

                        <View style={{padding: 4, margin: 4, elevation: 8, borderRadius: 35}}>
                            <Button
                                rounded
                                style={{
                                    width: 70, height: 70, justifyContent: 'space-around',
                                    alignItems: 'center', backgroundColor: '#122638'
                                }}
                                onPress={() => {
                                    ImagePicker.openPicker({
                                        width: 200,
                                        height: 200,
                                        cropping: true,
                                        includeBase64: true,
                                        cropperToolbarTitle: 'برش تصویر'
                                    }).then(newAvatar => {
                                        this.setState({
                                            newAvatar
                                        })
                                    });
                                }}
                            >
                                <Image style={{height: 60, width: 60}} source={galery}/>
                            </Button>
                        </View>
                    </View>

                    <Button
                        transparent full
                        style={{marginTop: 32}}
                        onPress={() => {
                            this.setState({
                                modalVisible: true
                            })
                        }}
                    >
                        <Text style={[styles.text, {fontSize: 24, textAlign: 'center', color: '#122638'}]}>
                            {this.state.newFName ? this.state.newFName + ' ' : this.state.userData.fname + ' '}
                            {this.state.newLName ? this.state.newLName : this.state.userData.lname}
                        </Text>
                    </Button>
                    <Text style={[styles.text, {fontSize: 16, textAlign: 'center', color: '#099992'}]}>
                        {this.state.userData.mobile}
                    </Text>

                    <Card style={{marginTop: 32}}>
                        <CardItem bordered>
                            <Body style={{flexDirection: 'row-reverse', justifyContent: 'space-between'}}>
                            <Text style={[styles.text, {fontSize: 16}]}>
                                شماره همراه
                            </Text>
                            <Text style={[styles.text, {fontSize: 16, color: '#09aaa2'}]}>
                                {this.state.userData.mobile}
                            </Text>
                            </Body>
                        </CardItem>
                        <CardItem bordered>
                            <Body style={{flexDirection: 'row-reverse', justifyContent: 'space-between'}}>
                            <Text style={[styles.text, {fontSize: 16}]}>
                                کد ملی
                            </Text>
                            <Text style={[styles.text, {fontSize: 16, color: '#09aaa2'}]}>
                                {this.state.userData.meliCode}
                            </Text>
                            </Body>
                        </CardItem>
                    </Card>

                    <View style={{
                        elevation: 8, marginTop: 8
                    }}>
                        <Button full
                                style={{backgroundColor: '#09aaa2'}}
                                onPress={() => {
                                    this.updateProfile().then(res => {
                                        if(res.status){
                                            Alert.alert('وضعیت بروزرسانی', 'اطلاعات پروفایل شما با موفقیت بروز شد. برای مشاهده تغییرات خارج و مجددا وارد اپ شوید.');
                                        }
                                        this.setState({
                                            sending: false
                                        })
                                    })
                                }}
                        >
                            <Text style={[styles.text, {width: '90%', color: '#fff', textAlign: 'right', fontSize: 16}]}>ذخیره و بروزرسانی
                                پروفایل</Text>

                            <Right style={{margin: 8}}>
                                {this.state.sending ?
                                    <Spinner color={Colors.COLOR_BASE_LIGHT}/> :
                                    <Image style={{height: 28, width: 28}} source={saveIcon}/>
                                }
                            </Right>
                        </Button>
                    </View>
                    <View style={{height: 60}}/>
                </Content>

                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.state.modalVisible}
                    onRequestClose={() => {
                        this.setState({
                            modalVisible: false
                        })
                    }}>
                    <TouchableOpacity
                        onPress={() =>
                            this.setState({
                                modalVisible: false
                            })
                        }
                        style={{
                            flex: 1,
                            justifyContent: 'center',
                            alignItems: 'center',
                            backgroundColor: '#00000099'
                        }}
                    >
                        <Form style={styles.modal}>
                            <View>
                                <Text style={styles.text}>ویرایش نام و نام خانوادگی</Text>
                            </View>
                            <Item floatingLabel>
                                <Label style={styles.text}>نام</Label>
                                <Input
                                    value={this.state.newFName}
                                    onChangeText={(newFName) => this.setState({newFName})}
                                />
                            </Item>
                            <Item floatingLabel last>
                                <Label style={styles.text}>نام خانوادگی</Label>
                                <Input
                                    value={this.state.newLName}
                                    onChangeText={(newLName) => this.setState({newLName})}
                                />
                            </Item>
                            <Button
                                full
                                style={{backgroundColor: '#09aaa2', marginTop: 16}}
                                onPress={() =>
                                    this.setState({
                                        modalVisible: false
                                    })
                                }
                            >
                                <Text style={styles.text}>تایید</Text>
                            </Button>
                        </Form>
                    </TouchableOpacity>
                </Modal>

            </Container>
        );
    }
}

const styles = StyleSheet.create({
    header: {
        backgroundColor: '#122638'
    },
    headerTitle: {
        fontFamily: 'sans',
        width: width - 140,
        direction: 'rtl',
        textAlign: 'right'
    },
    text: {
        fontSize: 14,
        fontFamily: 'sans'
    },
    avatar: {
        width: 140,
        height: 140,
        borderWidth: 5,
        borderColor: Colors.COLOR_BASE_LIGHT,
        alignSelf: 'center',
        borderRadius: 70
    },
    modal: {
        width: 320,
        backgroundColor: '#fff',
        padding: 16,
        margin: 8,
        elevation: 4
    }
});
