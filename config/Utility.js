// export const HOST = 'http://10.0.2.2:8000/api/v1/';
// export const API_MEDIA_AVATAR = 'http://10.0.2.2:8000/media/avatar/';

// export const HOST = 'http://192.168.1.212:8000/api/v1/';
// export const API_MEDIA_AVATAR = 'http://192.168.1.212:8000/media/avatar/';

export const HOST = 'http://sadrapayam.ir/api/v1/';
export const API_MEDIA_AVATAR = 'http://sadrapayam.ir/media/avatar/';

//colors list
export const Colors = {
    COLOR_BASE_DARK: '#05204A',
    COLOR_BASE_DARK2: '#B497D6',
    COLOR_BASE_DARK2_GLAS: '#B497D633',
    COLOR_BASE_LIGHT: '#E1E2EF',
    COLOR_BASE_LIGHT2: '#BFACAA',
    COLOR_BASE_LIGHT2_GLAS: '#05204A',
    COLOR_BASE_BLACK: '#BFACAA33',
    COLOR_STATUS_BAR: '#05225f',
    COLOR_BORDER_LIGHT: '#F3F3F3',
};

export default class Utility {

    static passGen = (leng: number) => {
        const charsLib = 'qwertyuiopasdfghjklzxcvbnm1234567890QWERTYUIOPASDFGHJKLZXCVBNM';
        let pass = '';
        for (let i = 0; i < leng; i++) {
            pass += charsLib.charAt(Math.floor(Math.random() * 62))
        }
        return pass;
    }
}
