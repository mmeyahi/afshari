import React from 'react';
import {I18nManager, TouchableHighlight} from 'react-native';
import {createDrawerNavigator, createStackNavigator} from 'react-navigation';
import Icon from 'react-native-vector-icons/Ionicons';

import ParentMenuScreen from '../screen/ParentMenuScreen';
import ParentScreen from '../screen/ParentScreen';
import SendMessageScreen from '../screen/parent/SendMessageScreen';
import CalendarScreen from '../screen/parent/CalendarScreen';
import InboxScreen from '../screen/parent/InboxScreen';
import OutboxScreen from '../screen/parent/OutboxScreen';
import ProfileScreen from '../screen/parent/ProfileScreen';

import DriverMenuScreen from '../screen/DriverMenuScreen';
import DriverScreen from '../screen/DriverScreen';
import TripScreen from '../screen/driver/TripScreen';
import DriverSendMessageScreen from '../screen/driver/SendMessageScreen';
import DriverInboxScreen from '../screen/driver/InboxScreen';
import DriverOutboxScreen from '../screen/driver/OutboxScreen';
import DriverProfileScreen from '../screen/driver/ProfileScreen';

import CaseScreen from '../screen/CaseScreen';

import DrawerContent from '../component/DrawerContent';
import {Colors} from './Utility';

const Parent = createDrawerNavigator({
    parentMenuScreen: {
        screen: ParentMenuScreen
    },
    parentScreen: {
        screen: ParentScreen
    },
    parentMsgScreen: {
        screen: SendMessageScreen
    },
    parentInbox: {
        screen: InboxScreen
    },
    parentOutbox: {
        screen: OutboxScreen
    },
    calendarScreen: {
        screen: CalendarScreen
    },
    parentProfileScreen: {
        screen: ProfileScreen
    }
}, {
    contentComponent: DrawerContent,
    drawerPosition: 'right',
    initialRouteName: 'parentMenuScreen',
    contentOptions: {
        itemsContainerStyle: {
            margin: 0,
            padding: 0
        },
        itemStyle: {
            flexDirection: 'row-reverse',
            borderBottomWidth: 1,
            borderBottomColor: Colors.COLOR_BASE_LIGHT,
            // backgroundColor: '#00002277'
        },
        labelStyle: {
            fontFamily: 'sans',
            fontWeight: 'normal',
            color: '#fff'
        },

        activeBackgroundColor: '#122638cc',
        inactiveBackgroundColor: '#09aaa2cc',

        activeTintColor: '#fff',
        inactiveTintColor: '#fff'
    }
});

const Driver = createDrawerNavigator({
    driverMenuScreen: {
        screen: DriverMenuScreen
    },
    driverScreen: {
        screen: DriverScreen
    },
    tripScreen: {
        screen: TripScreen
    },
    driverMsgScreen: {
        screen: DriverSendMessageScreen
    },
    driverInbox: {
        screen: DriverInboxScreen
    },
    driverOutbox: {
        screen: DriverOutboxScreen
    },
    driverProfileScreen: {
        screen: DriverProfileScreen
    }
}, {
    contentComponent: DrawerContent,
    drawerPosition: 'right',
    initialRouteName: 'driverMenuScreen',
    contentOptions: {
        itemsContainerStyle: {
            margin: 0,
            padding: 0
        },
        itemStyle: {
            flexDirection: 'row-reverse',
            borderBottomWidth: 1,
            borderBottomColor: Colors.COLOR_BASE_LIGHT,
            // backgroundColor: '#00002277'
        },
        labelStyle: {
            fontFamily: 'sans',
            fontWeight: 'normal',
            color: '#fff'
        },

        activeBackgroundColor: '#122638cc',
        inactiveBackgroundColor: '#09aaa2cc',

        activeTintColor: '#fff',
        inactiveTintColor: '#fff'
    }
});

const backButton = (navigation) => (
    <TouchableHighlight
        style={{width: 48, padding: 16}}
        underlayColor="transparent"
        onPress={() => {
            navigation.goBack();
        }}>
        <Icon name="ios-arrow-back" size={32} color="#fff"/>
    </TouchableHighlight>
);

const menuButton = (navigation) => (
    <TouchableHighlight
        style={{width: 48, padding: 16}}
        underlayColor="transparent"
        onPress={() => {
            navigation.toggleDrawer();
        }}>
        <Icon name={navigation.state.isDrawerOpen ? 'ios-close' : 'ios-menu'} size={32} color="#fff"/>
    </TouchableHighlight>
);

const Root = createStackNavigator({
        cases: CaseScreen,
        driver: Driver,
        parent: Parent
    },
    {
        initialRouteName: 'cases',
        headerMode: true,
        navigationOptions: ({navigation}) => ({
            headerStyle: {
                backgroundColor: Colors.COLOR_BASE_DARK
            },
            title: '',
            headerTintColor: 'white',
            headerTitleStyle: {
                fontWeight: 'bold',
            },
            headerRight: I18nManager.isRTL ? backButton(navigation) : menuButton(navigation),
            headerLeft: I18nManager.isRTL ? menuButton(navigation) : backButton(navigation)
        })
    });

export default Root;
